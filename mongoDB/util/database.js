const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient

let _db;

const mongoConnect = (callback) => {
  MongoClient.connect('mongodb+srv://gal:Aa123456@cluster0.wsywf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
  .then(client =>{
    console.log("connectd to DB")
    _db = client.db()
    callback(client)
  }).catch(err => {
    console.log(err)
    throw err
  })
}

const getDb = () => {
  if (_db) {
    return _db
  }

  throw 'No database found!'
}

exports.mongoConnect = mongoConnect
exports.getDb = getDb